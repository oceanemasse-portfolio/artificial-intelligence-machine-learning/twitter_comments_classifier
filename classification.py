# -*- coding: utf-8 -*-
# Author : Daudré--Treuil Prunelle & Masse Océane
# Avril 2020

#################################################### IMPORTS :

import pandas as pd
import copy
from collections import Counter
import time

from probability import *
from reading import *

#################################################### FUNCTIONS :

def classification(path_known, path_unknown):
    """
    Take a training set and a test set and return the percentage of correct
    predictions
    in: str, str
    out: float
    """

    ###### Training :
    p_pos, p_neg = p_pos_neg(path_known)
    n_corps = words_corpus(path_known)[0]
    d_occurence = count_words(words_corpus(path_known)[1])
    p_m = p_word(d_occurence, n_corps)
    n_pos, n_neg, mot_pos, mot_neg = words_pos_neg(path_known)
    occur_pos = count_words(mot_pos)
    occur_neg = count_words(mot_neg)
    p_m_pos = p_word(occur_pos, n_pos)
    p_m_neg = p_word(occur_neg, n_neg)

    ##### Prediction
    prediction = []

    set_test = read_tweets_unknow(path_unknown)
    tweets_test = clean_all_tweets(set_test[0])
    label_test = set_test[1]

    for elem_twt in tweets_test:
        p_pos_twt = p_pos # to computing the P(POS|L)
        p_m_pos_twt = 1

        p_neg_twt = p_neg # to computing the P(NEG|L)
        p_m_neg_twt = 1

        for elem_wrd in elem_twt:
            if (elem_wrd in mot_pos): # to computing the P(POS|L)
                p_pos_twt = p_pos_twt * p_m_pos.get(elem_wrd, 0)
                p_m_pos_twt = p_m_pos_twt * p_m.get(elem_wrd, 0)

            elif (elem_wrd in mot_neg): # to computing the P(NEG|L)
                p_neg_twt = p_neg_twt * p_m_neg.get(elem_wrd, 0)
                p_m_neg_twt = p_m_neg_twt * p_m.get(elem_wrd, 0)

        p_pos_twt = p_pos_twt / p_m_pos_twt
        p_neg_twt = p_neg_twt / p_m_neg_twt

        if (p_pos_twt > p_neg_twt):
            prediction.append("positive")
        else:
            prediction.append("negative")

    return score(prediction, label_test)

def score(label_predicted, label_known):
    """
    Compute the score of the lists label_predicted in comparaison to the list
    label_known
    in: list, list
    out: float
    """
    score = 0
    for i in range(0, len(label_predicted), 1):
        if (label_predicted[i] == label_known[i]):
            score = score + 1
    return score/len(label_known)

######################################################################### MAIN :

start_time = time.time()

print("Percentage of correct predictions with dev_set :",classification("./Sets/tweets_train.csv","./Sets/tweets_dev.csv"))
print("Percentage of correct predictions with test_set :",classification("./Sets/tweets_test.csv","./Sets/tweets_dev.csv"))

print("\n","Process time :  %s  s ---" % (time.time() - start_time))
